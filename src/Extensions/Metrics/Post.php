<?php


namespace PostInfoAnalyser\Extensions\Metrics;


use DateTime;
use DateTimeInterface;

class Post
{
    /* @var $id string */
    private $id;
    /* @var $from_name string */
    private $from_name;
    /* @var $from_id string */
    private $from_id;
    /* @var $message string */
    private $message;
    /* @var $type string */
    private $type;
    /* @var $created_time DateTime */
    private $created_time;

    public function __construct($id, $from_name, $from_id, $message, $type, $created_time)
    {
        $this->id = $id;
        $this->from_name = $from_name;
        $this->from_id = $from_id;
        $this->message = $message;
        $this->type = $type;
        $this->created_time = DateTime::createFromFormat(DateTimeInterface::W3C, $created_time);
    }

    public function getPost()
    {
        return array(
            "id" => $this->id,
            "from_name" => $this->from_name,
            "from_id" => $this->from_id,
            "message" => $this->message,
            "type" => $this->type,
            "created_time" => $this->created_time->format(DateTimeInterface::W3C)
        );
    }

    /**
     * @return DateTime
     */
    public function getCreatedTime()
    {
        return $this->created_time;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getFromId()
    {
        return $this->from_id;
    }

    /**
     * @return string
     */
    public function getFromName()
    {
        return $this->from_name;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getLengthOfPost()
    {
        return strlen($this->message);
    }

    /**
     * @return string
     */
    public function getYYYYMM()
    {
        return $this->created_time->format("Y/m");
    }
}