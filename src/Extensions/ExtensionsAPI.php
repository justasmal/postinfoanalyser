<?php


namespace PostInfoAnalyser\Extensions;

use Exception;
use PostInfoAnalyser\RedisControl;

class ExtensionsAPI
{
    protected $client;

    /**
     * @param $key
     * @param $value
     * @param null|int $timeout optional variable to set timeout for dataset saved in Redis
     */
    protected function cacheToRedis($key, $value, $timeout = null)
    {
        try {
            if ($timeout === null) {
                RedisControl::common()->set($key, $value);
            } else {
                RedisControl::common()->setex($key, $timeout, $value);
            }
        } catch (Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }

    }

    /**
     * @param $key
     * @return string|null
     */
    protected function getFromRedis($key)
    {
        $value = null;
        try {
            $value = RedisControl::common()->get($key);
        } catch (Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        } finally {
            return $value;
        }
    }
}