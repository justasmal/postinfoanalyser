<?php


namespace PostInfoAnalyser;

use FastRoute\Dispatcher;
use FastRoute\Dispatcher\GroupCountBased;
use FastRoute\RouteCollector;
use LogicException;
use PostInfoAnalyser\Controller\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;
use React\Http\Response;


class Router
{
    private $dispatcher;

    public function __construct(RouteCollector $routes)
    {
        $this->dispatcher = new GroupCountBased($routes->getData());
    }

    public function __invoke(ServerRequestInterface $request)
    {
        $routeInfo = $this->dispatcher->dispatch($request->getMethod(), $request->getUri()->getPath());
        switch ($routeInfo[0]) {
            case Dispatcher::NOT_FOUND:
                return JsonResponse::notFound("Not Found");//Response(404, ['Content-Type' => 'text/json'], '{"error":"Not found"}');
            case Dispatcher::METHOD_NOT_ALLOWED:
                $obj = new \stdClass();
                $obj->error = "Not Found";
                return JsonResponse::methodNotAllowed("Method not allowed");
            case Dispatcher::FOUND:
                $params = $routeInfo[2];
                return $routeInfo[1]($request, ... array_values($params));
        }
        throw new LogicException('Something wrong with routing');
    }
}