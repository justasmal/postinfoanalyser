<?php


namespace PostInfoAnalyser\Extensions\Metrics;


class CredentialsMetrics
{
    private $clientid;
    private $email;
    private $name;

    public function __construct($clientid, $email, $name)
    {
        $this->name = $name;
        $this->clientid = $clientid;
        $this->email = strtolower($email);
    }

    public function getSHA512()
    {
        $data = $this->parseAsArray(false);
        $serialized = json_encode($data);
        return hash('sha512', $serialized);
    }

    public function getAsArray()
    {
        return $this->parseAsArray();
    }

    private function parseAsArray($withname = true)
    {
        $arrayToReturn = array(
            'client_id' => $this->clientid,
            'email' => $this->email
        );
        if ($withname) {
            $arrayToReturn['name'] = $this->name;
        }
        return $arrayToReturn;
    }
}