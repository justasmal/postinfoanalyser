# PostInfoAnalyser

PostInfoAnalyser is a tool to crawl specific API. Unlike most of similar fashion tools using `get when I ask and it's done` approach it uses Redis to cache authorization and end results for a limited time(3530 and 300 seconds) thus avoid overusing that API request limits.


## How to start environment
- Install [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/)
- Edit `docker-compose.yml` environment variables if needed (optional)
- Open Terminal/PowerShell
-  Insert command `docker-compose up`
- Container is accessible through port 8000

## Usage

Send **POST** request to `127.0.0.1:8000/auth` (at least for Linux based OS, for Windows IP is different if using Docker Toolbox) with JSON request looking like:
```
{
	"client_id": "api_key",
	"email": "email@domain.tld",
	"name": "Name Surname"
}
```
