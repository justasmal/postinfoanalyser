<?php


namespace PostInfoAnalyser\Extensions\Metrics;


use DateInterval;
use DateTime;
use Exception;

class PostsData
{

    /* @var $objs Post[] */
    private $posts;

    public function __construct($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return Post[]
     */
    public function getPosts(): array
    {
        $postsArr = array();
        foreach ($this->posts as $post) {
            $postsArr[] = $post->getPost();
        }
        return $postsArr;
    }

    /**
     * @param Post[] $posts
     */
    public function addPosts(array $posts): void
    {
        $this->posts = array_merge($this->posts, $posts);
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post): void
    {
        $this->posts[] = $post;
    }

    private function sortByDate()
    {

        usort(
        /**
         * @param Post $a
         * @param Post $b
         * @return int
         */
            $this->posts, function ($a, $b) {
            return ($a->getCreatedTime() <=> $b->getCreatedTime());
        });
    }

    /**
     * @param $dateFormat string
     * @param $dateInterval DateInterval
     * @param null|string $append
     * @param bool $setweekbegin
     * @return array
     */
    private function generateArrayOfDatePatttern($dateFormat, $dateInterval, $append = null, $setweekbegin = false)
    {
        $this->sortByDate();
        $retArray = array();
        if (sizeof($this->posts) > 0) {
            /** @var DateTime $min
             * I was aware of that by default objects doesnt make hard copies
             * and writes stuff in same address space in memory, so I had to clone that
             */
            $min = clone $this->posts[0]->getCreatedTime();
            /** @var DateTime $max */
            $max = clone $this->posts[count($this->posts) - 1]->getCreatedTime();
            if ($setweekbegin === true) {
                $min->setISODate((int)$min->format('o'), (int)$min->format('W'), 1);
            } else {
                $min->modify('first day of this month')->setTime(0, 0, 0);
                $max->modify('first day of this month')->setTime(0, 0, 0);
            }

            while ($min <= $max) {
                $yearmonth = $min->format($dateFormat);
                $retArray[$append . $yearmonth] = 0;
                $min->add($dateInterval);
            }
        }
        return $retArray;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getEmptyArrayOfYYYYMM()
    {
        return $this->generateArrayOfDatePatttern("Ym", new DateInterval("P1M"));
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getEmptyArrayOfYYYYMMDDWeek()
    {
        return $this->generateArrayOfDatePatttern("Ymd", new DateInterval("P1W"), null, true);

    }

    public function getAvgCharLenByMonth()
    {
        try {
            $months = $this->getEmptyArrayOfYYYYMM();
            $postsCountByMonth = $months;
            foreach ($this->posts as $post) {
                $date = $post->getCreatedTime();
                $dateOfPost = $date->format("Ym");
                $months[$dateOfPost] += $post->getLengthOfPost();
                $postsCountByMonth[$dateOfPost]++;
            }
            foreach ($months as $date => $month) {
                if ($month > 0 && $postsCountByMonth[$date] > 0) {
                    $months[$date] = round($month / $postsCountByMonth[$date], 2);
                }
            }
            return $months;

        } catch (Exception $e) {
            return array();
        }
    }

    public function getLongestPostOfMonth()
    {
        $retArr = array();
        try {
            $months = $this->getEmptyArrayOfYYYYMM();
            $posts = $months;
            foreach ($months as $key => $month) {
                $months[$key] = "";
            }
            foreach ($this->posts as $post) {
                $date = $post->getCreatedTime();
                $dateOfPost = $date->format("Ym");
                if (strlen($months[$dateOfPost]) < $post->getLengthOfPost()) {
                    $months[$dateOfPost] = $post->getLengthOfPost();
                    $posts[$dateOfPost] = $post;
                }
            }
            foreach ($months as $key => $month) {
                /** @var Post[] $posts */
                $retArr[$key] = array(
                    "longestmessagechars" => $months[$key],
                    "message" => $posts[$key]->getMessage(),
                    "author" => $posts[$key]->getFromName()
                );
            }
        } catch (Exception $e) {
        }
        return $retArr;
    }

    public function getPostsByWeek()
    {
        try {
            $weeks = $this->getEmptyArrayOfYYYYMMDDWeek();
            foreach ($this->posts as $post) {
                $date = $post->getCreatedTime();
                $date->setISODate((int)$date->format('o'), (int)$date->format('W'), 1);
                $formatted = $date->format("Ymd");
                $weeks[$formatted]++;
            }
            return $weeks;
        } catch (Exception $e) {
        }
        return array();
    }

    public function getAvgPostPerUserByMonth()
    {
        try {
            $months = $this->getEmptyArrayOfYYYYMM();
            $postsCountByMonth = $months;
            $uniqueUsersByMonth = $months;
            foreach ($uniqueUsersByMonth as $key => $val) {
                $uniqueUsersByMonth[$key] = array();
            }
            foreach ($this->posts as $post) {
                $date = $post->getCreatedTime();
                $dateOfPost = $date->format("Ym");
                $postsCountByMonth[$dateOfPost]++;
                if (!in_array($post->getFromId(), $uniqueUsersByMonth[$dateOfPost])) {
                    $uniqueUsersByMonth[$dateOfPost][] = $post->getFromId();
                }
            }
            foreach ($months as $date => $month) {
                $users = sizeof($uniqueUsersByMonth[$date]);
                if ($postsCountByMonth[$date] > 0) {
                    $months[$date] = round($postsCountByMonth[$date] / $users, 2);
                }
            }
            return $months;

        } catch (Exception $e) {
            return array();
        }
    }

    public function getAnalysis()
    {
        try {
            $avgCharLenByMonth = $this->getAvgCharLenByMonth();
            $longestPostOfMonth = $this->getLongestPostOfMonth();
            $postsPerWeek = $this->getPostsByWeek();
            $avgPostPerUserByMonth = $this->getAvgPostPerUserByMonth();
            $data = array(
                "charlenbymonth" => $avgCharLenByMonth,
                "longestpostbyonth" => $longestPostOfMonth,
                "postsperweek" => $postsPerWeek,
                "avgpostuserbymonth" => $avgPostPerUserByMonth
            );
            return $data;
        } catch (Exception $exception) {
            return array();
        }
    }

}