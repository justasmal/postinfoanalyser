<?php


namespace PostInfoAnalyser\Controller;

use Error;
use Exception;
use PostInfoAnalyser\Extensions\Metrics\MetricsAPI;
use Psr\Http\Message\ServerRequestInterface;
use React\Promise;
use stdClass;

class DoThings
{
    /*
     * ExtensionsAPI
     */
    private $api;

    public function __construct()
    {
        //ExtensionsAPI component bootstrap
        $this->api = new MetricsAPI();
    }

    public function __invoke(ServerRequestInterface $request)
    {
        $signinData = json_decode((string)$request->getBody(), true);
        if (!$signinData) {
            return JsonResponse::badRequest("Request should be in JSON format");
        }
        $clientid = $signinData['client_id'] ?? '';
        $email = $signinData['email'] ?? '';
        $name = $signinData['name'] ?? '';
        $data = new stdClass();
        $data->clientid = $clientid;
        $data->email = $email;
        $data->name = $name;

        return $this->signIn($data)
            ->then(
                function ($data) {
                    return JsonResponse::ok($data);
                },
                function (Error $error) {
                    return JsonResponse::badRequest($error->getMessage());
                },
                function (Exception $error) {
                    return JsonResponse::badRequest($error->getMessage());
                }
            );
    }

    private function signIn($data)
    {
        if (empty($data->email)) {
            return Promise\reject(new Exception("Email is not set"));
        }
        if (empty($data->clientid)) {
            return Promise\reject(new Exception("Client ID is not set"));
        }
        if (empty($data->name)) {
            return Promise\reject(new Exception("Name is not set"));
        }
        try {
            $response = $this->api->signOn($data->clientid, $data->email, $data->name);
            return Promise\resolve($response);
        } catch (Exception $e) {
            var_dump($e->getMessage());
            return Promise\reject(new Exception("Something is wrong"));
        }
    }
}