<?php

use FastRoute\DataGenerator\GroupCountBased;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std;
use PostInfoAnalyser\Controller\DoThings;
use PostInfoAnalyser\RedisControl;
use PostInfoAnalyser\Router;
use React\Http\Server;
use React\MySQL\Factory;

require __DIR__ . '/vendor/autoload.php';

//env
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

//redis
$i = 0;
while ($i < 60) {
    if (RedisControl::getPong() === true) break;
    sleep(1);
    $i++;
    if ($i >= 60) {
        throw new Exception("Redis connection is not working");
    }
}

//Routes
$routes = new RouteCollector(new Std(), new GroupCountBased());
$routes->post('/auth', new DoThings());

//Server
$loop = \React\EventLoop\Factory::create();
$factory = new Factory($loop);
$server = new Server(new Router($routes));
$socket = new \React\Socket\Server('0.0.0.0:8000', $loop);
$server->listen($socket);
$server->on('error', function (Exception $exception) {
    echo $exception->getMessage() . PHP_EOL;
    echo $exception->getTraceAsString() . PHP_EOL;
});
echo 'Listening on ' . str_replace('tcp:', 'http:', $socket->getAddress()) . "\n";
$loop->run();