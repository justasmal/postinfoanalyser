<?php


namespace PostInfoAnalyser\Extensions\Metrics;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use PostInfoAnalyser\Extensions\ExtensionsAPI;
use PostInfoAnalyser\Extensions\ExtensionsInterface;
use stdClass;

class MetricsAPI extends ExtensionsAPI implements ExtensionsInterface
{

    private $sl_token;
    private $hash;

    public function __construct(int $timeout = 30)
    {
        $this->client = new Client([
            'base_uri' => "https://api.supermetrics.com",
            'timeout' => $timeout,
            'http_errors' => false
        ]);
    }

    public function signOn($clientid, $email, $name)
    {
        $returnObj = new stdClass();
        $signData = new CredentialsMetrics($clientid, $email, $name);
        $this->hash = $signData->getSHA512();
        $tokenhash = $this->getFromRedis($this->hash);
        if (!is_null($tokenhash)) {
            $returnObj->token = $tokenhash;
            $returnObj->data = $this->getPosts();
            $this->sl_token = $tokenhash;
            return $returnObj;
        }
        try {
            $response = $this->client->request('POST', 'assignment/register', [
                'form_params' => $signData->getAsArray()
            ]);
            echo $response->getStatusCode();
            $body = \GuzzleHttp\json_decode($response->getBody());
            switch ($response->getStatusCode()) {
                case 200:
                    if (isset($body->data->sl_token)) {
                        $returnObj->token = $body->data->sl_token;
                        $this->cacheToRedis($this->hash, $body->data->sl_token, 3530);
                        $this->sl_token = $body->data->sl_token;
                        $returnObj->data = $this->getPosts();
                        break;
                    } else {
                        $returnObj->error = "Unknown error";
                    }
                    break;
                case 400:
                case 500:
                    if (isset($body->error->message)) {
                        $returnObj->error = $body->error->message;
                    } else {
                        $returnObj->error = "MetricsAPI service issue";
                    }
                    break;
                default:
                    $returnObj->error = "MetricsAPI service issue";
                    break;
            }
        } catch (Exception $e) {
            $returnObj->error = "Unknown error";
            echo $e->getMessage() . PHP_EOL;
        } catch (GuzzleException $e) {
            $returnObj->error = "Unknown error";
            echo $e->getMessage() . PHP_EOL;
        } finally {
            return $returnObj;
        }
    }

    public function getPosts()
    {
        $analysis = $this->getFromRedis("analysis_" . $this->hash);

        if ($analysis) {
            $decoded = json_decode($analysis);
            return $decoded;
        }
        $page = 1;
        $curPage = -1;
        $posts = new PostsData([]);
        while (($page == $curPage + 1 || $curPage == -1)) {
            try {
                $response = $this->client->request('GET', 'assignment/posts', [
                    'query' => [
                        'sl_token' => $this->sl_token,
                        'page' => $page
                    ]
                ]);
                $body = \GuzzleHttp\json_decode($response->getBody());
                if (isset($body->data->page)) {
                    $curPage = $body->data->page;
                } else {
                    //stop crawling if issue arises
                    $curPage = -2;
                }
                foreach ($body->data->posts as $post) {
                    $newPost = new Post($post->id, $post->from_name, $post->from_id, $post->message, $post->type, $post->created_time);
                    $posts->addPost($newPost);
                }

                $page++;
            } catch (GuzzleException $e) {
                $curPage = -2;
            }

        }
        $data = $posts->getAnalysis();
        $this->cacheToRedis("analysis_" . $this->hash, json_encode($data), 300);
        return $data;
    }
}