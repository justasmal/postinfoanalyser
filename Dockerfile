FROM php:7.3-alpine

RUN apk add --no-cache zip libzip-dev && docker-php-ext-install zip

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
# multithreaded fetching :)
RUN composer global require hirak/prestissimo